# PHP Ant Tasks #

Ant tasks for Php development.

These tasks are currently only being developed and tested for ant in eclipse/PDT

### Purpose ###

Current tasks are

* updateconfig (for updating local wordpress config files)
* deploy (for handling various tasks when deploying to a remote server via ssh)
* dumptable
* difftablecontent

### Requirements ###

This library is using Jsch, which is included. Liquibase and the mysql-connector is used experimentally for handling Wordpress's irritating database usage. These are not included and must at this point be added to the classpath manually.

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact