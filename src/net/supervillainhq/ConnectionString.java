package net.supervillainhq;

public class ConnectionString {
	private String user;
	private String host;
	private int port = 22;

	public ConnectionString(){
		this("zupa@dylan.zuparecommended.dk:2200");
	}
	public ConnectionString(String string){
		int posAt = string.indexOf('@');
		int posKolon = string.indexOf(':');
		String user = string.substring(0, posAt);
		String host = string.substring(posAt, posKolon);
		String port = string.substring(posKolon, string.length());
		setHost(host);
		setPort(Integer.getInteger(port));
		setUser(user);
	}
	
	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	
	public String toString(){
		return (null==user ? "" : user) + "@" + (null==host ? "" : host) + ":" + Integer.toString(port);
	}
}
