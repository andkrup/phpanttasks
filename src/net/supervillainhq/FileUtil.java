package net.supervillainhq;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;

public class FileUtil {
	public static boolean createFolder(String foldername){
		File folder = new File(foldername);
		return createFolder(folder);
	}
	public static boolean createFolder(File folder){
		File file = new File(folder.getAbsolutePath());
		if(file.isDirectory()){
			System.out.println("folder already exists: "+folder.getAbsolutePath());
			return false;
		}
		try{
			if(!file.mkdir()){
				System.out.println("failed to create folder: "+folder.getAbsolutePath());
			}
		}
		catch(Exception e){
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static void writeToFile(String filename) {
		File file = new File(filename);
		writeToFile("empty", file, "UTF-8");
	}
	public static void writeToFile(String text, String filename) {
		File file = new File(filename);
		writeToFile(text, file, "UTF-8");
	}
	public static void writeToFile(String text, File file) {
		writeToFile(text, file, "UTF-8");
	}
	public static void writeToFile(String text, File file, String encoding) {
		writeToFile(text, file, encoding, false);
	}
	public static void writeToFile(String text, File file, String encoding, boolean verbose) {
		if(!file.exists()){
			try {
				file.createNewFile();
			}
			catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		String filename = file.getAbsolutePath();
		FileOutputStream filestream = null;
		OutputStreamWriter streamwriter = null;
		try {
			filestream = new FileOutputStream(filename);
			streamwriter = new OutputStreamWriter(filestream, encoding);
		}
		catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Writer writer = null;
		if(null!=streamwriter){
			writer = new BufferedWriter(streamwriter);
			if(verbose){
				System.out.println("Writing to file ("+filename+") contents["+text.length()+"]: "+text);
			}
			try {
				writer.write(text);
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			finally{
				if(null!=writer){
					try {
						writer.close();
					}
					catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		}
	}
}
