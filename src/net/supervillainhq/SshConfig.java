package net.supervillainhq;

import java.io.File;

import org.apache.tools.ant.Project;


public class SshConfig {
	private String identityPath;
	private String knownHostsPath;
	private String user;
	private String host;
	private int port;
	
	public SshConfig(ConnectionString connection){
		this(connection.getHost(), connection.getPort(), connection.getUser());
	}
	public SshConfig(){
		this("127.0.0.1");
	}
	public SshConfig(String host){
		this(host, 22);
	}
	public SshConfig(String host, int port){
		this(host, port, "user");
	}
	public SshConfig(String host, int port, String user){
		this(host, port, user, "~/.ssh/id_rsa");
	}
	public SshConfig(String host, int port, String user, String identityPath){
		this(host, port, user, identityPath, "~/.ssh/known_hosts");
	}
	public SshConfig(String host, int port, String user, String identityPath, String knownhostsPath){
		setHost(host);
		setPort(port);
		setUser(user);
		setIdentityFile(identityPath);
		setKnownHostsFile(knownhostsPath);
	}
	
	
	public static SshConfig fromfile(Project project, String filepath){
		SshConfigBuilder builder = new SshConfigBuilder(project);
		return builder.createFromPropertyFile(filepath);
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}
	public String getIdentityFile() {
		if (identityPath.startsWith("~" + File.separator)) {
			return System.getProperty("user.home") + identityPath.substring(1);
		}
		return identityPath;
	}
	public void setIdentityFile(String value) {
		this.identityPath = value;
	}
	public String getKnownHostsFile() {
		if (knownHostsPath.startsWith("~" + File.separator)) {
			return System.getProperty("user.home") + knownHostsPath.substring(1);
		}
		return knownHostsPath;
	}
	public void setKnownHostsFile(String knownHostsPath) {
		this.knownHostsPath = knownHostsPath;
	}
}
