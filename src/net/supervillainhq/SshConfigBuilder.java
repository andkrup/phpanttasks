package net.supervillainhq;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Property;

public class SshConfigBuilder extends Property {
	public SshConfigBuilder(Project project){
		setProject(project);
	}
	
	public SshConfig createFromPropertyFile(String path) {
		loadFile(new File(path));
		Project project = getProject();
		SshConfig sshconfig = new SshConfig();
		sshconfig.setHost(project.getProperty("ssh.host"));
		String identity = project.getProperty("ssh.keyfile");
		if(null==identity || identity == ""){
			identity = "~/.ssh/id_rsa";
		}
		sshconfig.setIdentityFile(identity);
		String knownhosts = project.getProperty("ssh.knownhosts");
		if(null==knownhosts || knownhosts == ""){
			knownhosts = "~/.ssh/known_hosts";
		}
		sshconfig.setKnownHostsFile(knownhosts);
		sshconfig.setPort(Integer.parseInt(project.getProperty("ssh.port")));
		sshconfig.setUser(project.getProperty("ssh.user"));
		
		return sshconfig;
	}
}
