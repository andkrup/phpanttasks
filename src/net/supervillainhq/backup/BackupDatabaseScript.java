package net.supervillainhq.backup;

import net.supervillainhq.FileUtil;
import net.supervillainhq.wordpress.DatabaseCredentials;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

public class BackupDatabaseScript {
	private Project project;
	private String outputfile;
	private String databasePropertiesfile;

	public BackupDatabaseScript() {
		this(null);
	}
	public BackupDatabaseScript(Project project) {
		this(project, null);
	}
	public BackupDatabaseScript(Project project, String databasePropertiesfile) {
		this.setProject(project);
		this.setDatabaseProperties(databasePropertiesfile);
	}
	
	public void update(){
		this.update(null);
	}

	public void update(DatabaseCredentials dbCreds){
		if(null==getProject()){
			throw new BuildException("no project reference");
		}
		if(null==dbCreds){
			if(null==databasePropertiesfile){
				throw new BuildException("no database properties file reference");
			}
			dbCreds = DatabaseCredentials.fromPropertyfile(getProject(), databasePropertiesfile);
		}
		
		String database = dbCreds.getDatabase();
		String dbUser = dbCreds.getUser();
		String dbPass = dbCreds.getPass();
		
		String contents = "#!/bin/bash\n" +
				"# backup database to user zupas backup folder\n" +
				"mysqldumpcmd=`which mysqldump`\n" +
				"tarcmd=`which tar`\n" +
				"\"$mysqldumpcmd\" -u" + dbUser + " -p" + dbPass + " " + database + " > ~/backup/" + database + ".sql\n" +
				"\"$tarcmd\" -czvf ~/backup/" + database + ".sql.tar.gz ~/backup/" + database + ".sql\n" +
				"rm ~/backup/" + database + ".sql\n";
		FileUtil.writeToFile(contents, getOutputfile());
	}

	public String getOutputfile() {
		return outputfile;
	}

	public void setOutputfile(String outputfile) {
		this.outputfile = outputfile;
	}
	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	public String getDatabaseProperties() {
		return databasePropertiesfile;
	}

	public void setDatabaseProperties(String databasePropertiesfile) {
		this.databasePropertiesfile = databasePropertiesfile;
	}
}
