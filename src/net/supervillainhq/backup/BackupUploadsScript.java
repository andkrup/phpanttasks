package net.supervillainhq.backup;

import net.supervillainhq.FileUtil;
import net.supervillainhq.wordpress.DatabaseCredentials;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

public class BackupUploadsScript {
	private Project project;
	private String outputfile;
	private String databasePropertiesfile;
	private String remoteBackupPath;

	public BackupUploadsScript() {
		this(null, null, null);
	}
	public BackupUploadsScript(Project project) {
		this(project, null, null);
	}
	public BackupUploadsScript(Project project, String databasePropertiesfile) {
		this(project, databasePropertiesfile, null);
	}
	public BackupUploadsScript(Project project, String databasePropertiesfile, String remoteBackupPath) {
		this.setProject(project);
		this.setDatabaseProperties(databasePropertiesfile);
		this.setRemoteBackupPath(remoteBackupPath);
	}
	
	public void update(){
		this.update(null, null);
	}
	public void update(DatabaseCredentials dbCreds){
		this.update(dbCreds, null);
	}
	public void update(DatabaseCredentials dbCreds, String remoteBackupPath){
		if(null==getProject()){
			throw new BuildException("no project reference");
		}
		if(null==dbCreds){
			if(null==databasePropertiesfile){
				throw new BuildException("no database properties file reference");
			}
			dbCreds = DatabaseCredentials.fromPropertyfile(getProject(), databasePropertiesfile);
		}
		if(null==remoteBackupPath){
			remoteBackupPath = getRemoteBackupPath();
		}
		
		String database = dbCreds.getDatabase();
		
		String contents = "#!/bin/bash\n" +
				"# backup uploads folder contents to user zupas backup folder\n" +
				"tarcmd=`which tar`\n" +
				"\"$tarcmd\" -czvf ~/backup/" + database + ".uploads.tar.gz " + remoteBackupPath + "/uploads\n";
		FileUtil.writeToFile(contents, getOutputfile());
	}

	public String getOutputfile() {
		return outputfile;
	}

	public void setOutputfile(String outputfile) {
		this.outputfile = outputfile;
	}
	
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}
	
	public String getDatabaseProperties() {
		return databasePropertiesfile;
	}

	public void setDatabaseProperties(String databasePropertiesfile) {
		this.databasePropertiesfile = databasePropertiesfile;
	}
	
	public String getRemoteBackupPath() {
		return remoteBackupPath;
	}

	public void setRemoteBackupPath(String remoteBackupPath) {
		this.remoteBackupPath = remoteBackupPath;
	}
}
