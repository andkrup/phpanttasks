package net.supervillainhq.backup;

import java.io.File;

import net.supervillainhq.deployment.ISshCommand;
import net.supervillainhq.deployment.UpdateRemoteFile;

import org.apache.tools.ant.Project;

public class UpdateBackupDatabaseScript extends UpdateRemoteFile implements ISshCommand {
	private BackupDatabaseScript backupDbScript;
	private UpdateRemoteFile remotefile;
	private boolean saveLocal = true;

	public UpdateBackupDatabaseScript(Project project) {
		super(project);
		backupDbScript = new BackupDatabaseScript(project);
		backupDbScript.setOutputfile(project.getBaseDir().getAbsolutePath() + "/scripts/backup-database");
		remotefile = new UpdateRemoteFile(project);
	}


	public void execute() throws Exception {
		File file = new File(backupDbScript.getOutputfile()).getAbsoluteFile();
		File parentDir = new File(file.getParentFile().getAbsolutePath());
		if(file.canWrite() || parentDir.canWrite()){
			backupDbScript.setProject(project);
			// create temp local wp-config.php, based on properties file
			backupDbScript.update();
			// rsync to remote
			remotefile.setSshconfigpath(sshconfigpath);
			remotefile.setLocalFilepath(backupDbScript.getOutputfile());
			remotefile.execute();
			// delete temp local wp-config.php
			if(!saveLocal){
				file.delete();
			}
		}
		else{
			throw new Exception(file.toString() + " is not writable or the folder " + parentDir.toString() + " doesn't exist");
		}
	}

	public String getLocalFilepath() {
		return backupDbScript.getOutputfile();
	}

	public void setLocalFilepath(String localFilepath) {
		this.backupDbScript.setOutputfile(localFilepath);
	}

	public String getRemotePath() {
		return remotefile.getRemotePath();
	}

	public void setRemotePath(String remotePath) {
		this.remotefile.setRemotePath(remotePath);
	}

	
	public String getDatabaseProperties() {
		return backupDbScript.getDatabaseProperties();
	}

	public void setDatabaseProperties(String databasePropertiesfile) {
		this.backupDbScript.setDatabaseProperties(databasePropertiesfile);
	}


	public boolean isSaveLocal() {
		return saveLocal;
	}


	public void setSaveLocal(boolean saveLocal) {
		this.saveLocal = saveLocal;
	}
}
