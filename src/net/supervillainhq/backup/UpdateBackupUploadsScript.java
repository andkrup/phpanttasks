package net.supervillainhq.backup;

import java.io.File;

import net.supervillainhq.deployment.ISshCommand;
import net.supervillainhq.deployment.UpdateRemoteFile;

import org.apache.tools.ant.Project;

public class UpdateBackupUploadsScript extends UpdateRemoteFile implements ISshCommand {
	private BackupUploadsScript backupScript;
	private UpdateRemoteFile remotefile;
	private boolean saveLocal = true;

	public UpdateBackupUploadsScript(Project project) {
		super(project);
		backupScript = new BackupUploadsScript(project);
		backupScript.setOutputfile(project.getBaseDir().getAbsolutePath() + "/scripts/backup-uploads");
		remotefile = new UpdateRemoteFile(project);
	}


	public void execute() throws Exception {
		File file = new File(backupScript.getOutputfile()).getAbsoluteFile();
		File parentDir = new File(file.getParentFile().getAbsolutePath());
		if(file.canWrite() || parentDir.canWrite()){
			backupScript.setProject(project);
			// create temp local wp-config.php, based on properties file
			backupScript.update();
			// rsync to remote
			remotefile.setSshconfigpath(sshconfigpath);
			remotefile.setLocalFilepath(backupScript.getOutputfile());
			remotefile.execute();
			// delete temp local wp-config.php
			if(!saveLocal){
				file.delete();
			}
		}
		else{
			throw new Exception(file.toString() + " is not writable or the folder " + parentDir.toString() + " doesn't exist");
		}
	}

	public String getLocalFilepath() {
		return backupScript.getOutputfile();
	}

	public void setLocalFilepath(String localFilepath) {
		this.backupScript.setOutputfile(localFilepath);
	}

	public String getRemotePath() {
		return remotefile.getRemotePath();
	}

	public void setRemotePath(String remotePath) {
		this.remotefile.setRemotePath(remotePath);
	}

	public String getRemoteBackupPath() {
		return backupScript.getRemoteBackupPath();
	}
	
	public void setRemoteBackupPath(String remoteBackupPath) {
		this.backupScript.setRemoteBackupPath(remoteBackupPath);
	}
	
	public String getDatabaseProperties() {
		return backupScript.getDatabaseProperties();
	}

	public void setDatabaseProperties(String databasePropertiesfile) {
		this.backupScript.setDatabaseProperties(databasePropertiesfile);
	}


	public boolean isSaveLocal() {
		return saveLocal;
	}


	public void setSaveLocal(boolean saveLocal) {
		this.saveLocal = saveLocal;
	}
}

