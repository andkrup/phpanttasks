package net.supervillainhq.db;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Property;

public class DatabaseConfig extends Property{
	public String database;
	public String username;
	public String password;
	public String charset;
	public String host;
	public int port;
	public String collate;
	
	public DatabaseConfig(){
		this.charset = "utf8";
		this.host = "127.0.0.1";
		this.port = 3306;
	}
	
	static public DatabaseConfig fromConfigFile(Project project, File filepath){
		DatabaseConfig instance = new DatabaseConfig();
		instance.setProject(project);
		File absfile = filepath.getAbsoluteFile();
		instance.loadFile(absfile);
		
		instance.database = (project.getProperty("db.database"));
		instance.username = (project.getProperty("db.user"));
		instance.password = (project.getProperty("db.pass"));
		instance.host = (project.getProperty("db.host"));
		instance.charset = (project.getProperty("db.charset"));
		instance.port = Integer.parseInt(project.getProperty("db.port"));
		instance.collate = project.getProperty("db.collate");
		return instance;
	}
}
