package net.supervillainhq.deployment;

import java.io.InputStream;

import org.apache.tools.ant.Project;

import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;

public class ClearRemoteFolder extends SshCommand implements ISshCommand {
	private String remotePath;

	public ClearRemoteFolder(Project project) {
		super(project);
	}

	public void execute(){
		sshconfig = validateSshConfig();
		String passphrase = project.getProperty("ssh.key.phrase");
        String identityPath = sshconfig.getIdentityFile();
        String knownHosts = sshconfig.getKnownHostsFile();
        String user = sshconfig.getUser();
        String host = sshconfig.getHost();
        int port = sshconfig.getPort();
        
        command = "cd " + remotePath + ";rm -r " + remotePath + ";mkdir " + remotePath + ";";
        
	    try{
	        JSch jsch=new JSch();

	        Session session = jsch.getSession(user, host, port);

	        jsch.setKnownHosts(knownHosts);
			jsch.addIdentity(identityPath, passphrase);

	        session.connect();

	        Channel channel = session.openChannel("exec");
	        ((ChannelExec)channel).setCommand(command);

	        channel.setInputStream(null);

	        ((ChannelExec)channel).setErrStream(System.err);

	        InputStream in = channel.getInputStream();

	        channel.connect();
	        
			byte[] tmp = new byte[1024];
			while(true){
				while(in.available()>0){
					int i = in.read(tmp, 0, 1024);
					if(i<0){
						break;
					}
					System.out.print(new String(tmp, 0, i));
				}
				if(channel.isClosed()){
					System.out.println("exit-status: "+channel.getExitStatus());
					break;
				}
				try{
					Thread.sleep(1000);
				}
				catch(Exception ee){
					ee.printStackTrace();
				}
	        }
	        channel.disconnect();
	        session.disconnect();
	    }
	    catch(JSchException jschE){
	    	System.out.println("Error: "+jschE+"\nCause: "+jschE.getCause());
		}
		catch(Exception e){
			System.out.println("Error: "+e);
		}
	}

	public String getRemotePath() {
		return remotePath;
	}

	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}
}
