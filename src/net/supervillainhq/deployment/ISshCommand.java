package net.supervillainhq.deployment;

import org.apache.tools.ant.Project;

public interface ISshCommand {
	void execute() throws Exception;
	void setProject(Project project);
	void setSshconfigpath(String sshconfigpath);
}
