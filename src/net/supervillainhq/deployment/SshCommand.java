package net.supervillainhq.deployment;

import net.supervillainhq.SshConfig;

import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;

public class SshCommand implements ISshCommand {
	protected Project project;
	protected SshConfig sshconfig;
	protected String sshconfigpath;
	protected String command;

	public SshCommand(Project project){
		this.project = project;
	}

	protected SshConfig validateSshConfig() throws BuildException{
		String passphrase = project.getProperty("ssh.key.phrase");
		if(null==passphrase){
			System.out.println("This build is assuming your identity key doesn't require a pass-phrase. Please create the property 'ssh.key.phrase' with the pass-phrase for your identity key-file in the current project if the key requires a pass-phrase");
		}
		if(null==sshconfigpath){
			throw new BuildException("Attribute sshconfigpath is required");
		}
		return SshConfig.fromfile(project, sshconfigpath);
	}

	
	public void execute() throws Exception {
	}
	
	public SshConfig getSshconfig() {
		return sshconfig;
	}
	public void setSshconfig(SshConfig sshconfig) {
		this.sshconfig = sshconfig;
	}
	
	public String getSshconfigpath() {
		return sshconfigpath;
	}
	public void setSshconfigpath(String sshconfigpath) {
		this.sshconfigpath = sshconfigpath;
	}

	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public String getCommand() {
		return command;
	}

	public void setCommand(String command) {
		this.command = command;
	}
}
