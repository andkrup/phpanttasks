package net.supervillainhq.deployment;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.optional.ssh.SSHExec;

public class SshExecCommand extends SshCommand{
	
	public SshExecCommand(Project project){
		super(project);
		command = "cd ~;ls -la;cd /var/www/www-publisher;ls -la";
	}
	
	public void execute(){
		sshconfig = validateSshConfig();
		String passphrase = project.getProperty("ssh.key.phrase");
        
        String identityPath = sshconfig.getIdentityFile();
        String knownHosts = sshconfig.getKnownHostsFile();
        String user = sshconfig.getUser();
        String host = sshconfig.getHost();
        int port = sshconfig.getPort();
		
		SSHExec sshexec = new SSHExec();
		sshexec.setKeyfile(identityPath);
		sshexec.setKnownhosts(knownHosts);
		sshexec.setUsername(user);
		sshexec.setHost(host);
		sshexec.setPort(port);
		if(null!=passphrase){
			// not setting it if the private key has no passphrase
			sshexec.setPassphrase(passphrase);
		}
		
		sshexec.setCommand(command);
	}
}
