package net.supervillainhq.deployment;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline.Argument;

public class UpdateRemoteFile extends SshCommand implements ISshCommand {
	private String remotePath;
	private String localFilepath;

	public UpdateRemoteFile(Project project){
		super(project);
	}
	
	
	public void execute() throws Exception {
		sshconfig = validateSshConfig();
        String identityPath = sshconfig.getIdentityFile();
        String user = sshconfig.getUser();
        String host = sshconfig.getHost();
        int port = sshconfig.getPort();
        

		File file = new File(localFilepath).getAbsoluteFile();
		if(file.isFile()){
			//rsync -avz -e "ssh -i /Users/zr-ank/.ssh/id_rsa -p 2200 " /Users/zr-ank/git/ZupaAntTasks/testupdateanttask/.frisk www-publisher@dylan.zuparecommended.dk:/home/www-publisher/testupdateanttask/
			ExecTask exec = new ExecTask();
			exec.setProject(project);
			exec.setDir(project.getBaseDir());
			exec.setExecutable("rsync");
			
			Argument arg;
			arg = exec.createArg();
			arg.setLine("-avz");
			
			arg = exec.createArg();
			arg.setLine("-e \"ssh -i " + identityPath + " -p " + port + "\"");
			
			arg = exec.createArg();
			arg.setLine("--omit-dir-times");
			arg.setLine("--times");
			arg = exec.createArg();
			arg.setLine("--perms");
			arg.setLine("--owner");
			arg = exec.createArg();
			arg.setLine("--group");
			
			arg = exec.createArg();
			arg.setLine(localFilepath);
			
			arg = exec.createArg();
			arg.setLine(user + "@" + host + ":" + (remotePath.endsWith("/") ? remotePath : remotePath + "/"));
			
			exec.execute();
		}
		
	}

	public String getLocalFilepath() {
		return localFilepath;
	}

	public void setLocalFilepath(String localFilepath) {
		this.localFilepath = localFilepath;
	}

	public String getRemotePath() {
		return remotePath;
	}

	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}
}
