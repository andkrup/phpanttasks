package net.supervillainhq.deployment;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline.Argument;

public class UpdateRemoteFiles extends SshCommand implements ISshCommand {
	private String remotePath;
	private String localPath;
	private String excludes;
	private String includes;
	private boolean deleteExcluded;

	public UpdateRemoteFiles(Project project){
		super(project);
	}
	
	public void execute() throws Exception {
		sshconfig = validateSshConfig();
        String identityPath = sshconfig.getIdentityFile();
        String user = sshconfig.getUser();
        String host = sshconfig.getHost();
        int port = sshconfig.getPort();
        

		File file = new File(new File(localPath).getAbsolutePath());
		if(file.isDirectory()){
			//rsync -avzd -e "ssh -i /Users/zr-ank/.ssh/id_rsa -p 2200 " --exclude='*/' --exclude='*.txt' --delete  /Users/zr-ank/git/ZupaAntTasks/testupdateanttask/ www-publisher@dylan.zuparecommended.dk:/home/www-publisher/testupdateanttask/
			ExecTask exec = new ExecTask();
			exec.setProject(project);
			exec.setDir(project.getBaseDir());
			exec.setExecutable("rsync");
			
			Argument arg;
			arg = exec.createArg();
			arg.setLine("-avzd");
			
			arg = exec.createArg();
			arg.setLine("-e \"ssh -i " + identityPath + " -p " + port + "\"");
			
			arg = exec.createArg();
			arg.setLine("--exclude='*/'");

			if(null!=excludes && excludes.length()>0){
				String[] excludeslist = excludes.split(",");
				for (String exclude : excludeslist) {
					arg = exec.createArg();
					arg.setLine("--exclude='" + exclude + "'");
				}
			}
			if(null!=includes && includes.length()>0){
				String[] includeslist = includes.split(",");
				for (String include : includeslist) {
					arg = exec.createArg();
					arg.setLine("--include='" + include + "'");
				}
			}
			
			if(deleteExcluded){
				arg = exec.createArg();
				arg.setLine("--delete-excluded");
			}
			arg = exec.createArg();
			arg.setLine("--omit-dir-times");
			arg.setLine("--times");
			arg = exec.createArg();
			arg.setLine("--perms");
			arg = exec.createArg();
			arg.setLine("--owner");
			arg = exec.createArg();
			arg.setLine("--group");
			
			arg = exec.createArg();
			arg.setLine(localPath.endsWith("/") ? localPath : localPath + "/");
			
			arg = exec.createArg();
			arg.setLine(user + "@" + host + ":" + (remotePath.endsWith("/") ? remotePath : remotePath + "/"));
			
			exec.execute();
		}
		
	}

	public String getlocalPath() {
		return localPath;
	}

	public void setlocalPath(String localPath) {
		this.localPath = localPath;
	}

	public String getRemotePath() {
		return remotePath;
	}

	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}

	public String getExclude() {
		return excludes;
	}

	public void setExclude(String excludes) {
		this.excludes = excludes;
	}

	public String getInclude() {
		return includes;
	}

	public void setInclude(String includes) {
		this.includes = includes;
	}

	public boolean isDeleteExcluded() {
		return deleteExcluded;
	}

	public void setDeleteExcluded(boolean deleteExcluded) {
		this.deleteExcluded = deleteExcluded;
	}
}
