package net.supervillainhq.deployment;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline.Argument;

public class UpdateRemoteFolder extends SshCommand implements ISshCommand {
	private boolean verbose;
	private String remotePath;
	private String excludes;
	private String includes;
	private String localPath;
	private boolean deleteExcluded;
	
	public UpdateRemoteFolder(Project project){
		super(project);
	}
	
	/**
	 * Remove target-folder, then upload from source-folder
	 * @throws Exception 
	 */
	public void execute() throws Exception{
		sshconfig = validateSshConfig();
        String identityPath = sshconfig.getIdentityFile();
        String user = sshconfig.getUser();
        String host = sshconfig.getHost();
        int port = sshconfig.getPort();
        
        //rsync -r -e "ssh -i /Users/zr-ank/.ssh/id_rsa -p 2200 " --delete /Users/zr-ank/git/ZupaAntTasks/testupdateanttask/ www-publisher@dylan.zuparecommended.dk:/home/www-publisher/testupdateanttask/
        ExecTask exec = new ExecTask();
        exec.setProject(project);
		exec.setDir(project.getBaseDir());
		exec.setExecutable("rsync");

		Argument arg;
		
		if(verbose){
			arg = exec.createArg();
			arg.setLine("-vr");
		}
		else{
			arg = exec.createArg();
			arg.setLine("-r");
		}
		
		arg = exec.createArg();
		arg.setLine("--delete");
		
		arg = exec.createArg();
		arg.setLine("-e \"ssh -i " + identityPath + " -p " + port + "\"");

		if(null!=excludes && excludes.length()>0){
			String[] excludeslist = excludes.split(",");
			for (String exclude : excludeslist) {
				arg = exec.createArg();
				arg.setLine("--exclude='" + exclude + "'");
			}
		}
		if(null!=includes && includes.length()>0){
			String[] includeslist = includes.split(",");
			for (String include : includeslist) {
				arg = exec.createArg();
				arg.setLine("--include='" + include + "'");
			}
		}
		
		if(deleteExcluded){
			arg = exec.createArg();
			arg.setLine("--delete-excluded");
		}
		arg = exec.createArg();
		arg.setLine("--omit-dir-times");
		arg = exec.createArg();
		arg.setLine("--times");
		arg = exec.createArg();
		arg.setLine("--perms");
		arg.setLine("--owner");
		arg = exec.createArg();
		arg.setLine("--group");
		
		arg = exec.createArg();
		arg.setLine(localPath.endsWith("/") ? localPath : localPath + "/");
		
		arg = exec.createArg();
		arg.setLine(user + "@" + host + ":" + (remotePath.endsWith("/") ? remotePath : remotePath + "/"));
		
		exec.execute();
	}

	public String getRemotePath() {
		return remotePath;
	}

	public void setRemotePath(String remotePath) {
		this.remotePath = remotePath;
	}

	public String getLocalPath() {
		return localPath;
	}

	public void setLocalPath(String localPath) {
		this.localPath = localPath;
	}

	public String getExclude() {
		return excludes;
	}

	public void setExclude(String excludes) {
		this.excludes = excludes;
	}

	public String getInclude() {
		return includes;
	}

	public void setInclude(String includes) {
		this.includes = includes;
	}

	public boolean isDeleteExcluded() {
		return deleteExcluded;
	}

	public void setDeleteExcluded(boolean deleteExcluded) {
		this.deleteExcluded = deleteExcluded;
	}

	public boolean isVerbose() {
		return verbose;
	}

	public void setVerbose(boolean verbose) {
		this.verbose = verbose;
	}
}
