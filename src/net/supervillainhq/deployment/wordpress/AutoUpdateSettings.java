package net.supervillainhq.deployment.wordpress;

public enum AutoUpdateSettings {
	DISABLED, CORE, ENABLED
}
