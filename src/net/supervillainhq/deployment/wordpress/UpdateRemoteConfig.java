package net.supervillainhq.deployment.wordpress;

import java.io.File;

import net.supervillainhq.deployment.ISshCommand;
import net.supervillainhq.deployment.UpdateRemoteFile;

import org.apache.tools.ant.Project;

public class UpdateRemoteConfig extends UpdateRemoteFile implements ISshCommand{
	private UpdateWordpressConfig wpconf;
	private UpdateRemoteFile remotefile;
	private String lang;

	public UpdateRemoteConfig(Project project) {
		super(project);
		wpconf = new UpdateWordpressConfig(project);
		wpconf.setOutputfile("/tmp/wp-config.php");
		remotefile = new UpdateRemoteFile(project);
	}

	public void execute() throws Exception {
		wpconf.setProject(project);
		// create temp local wp-config.php, based on properties file
		wpconf.update(lang);
		// rsync to remote
		remotefile.setSshconfigpath(sshconfigpath);
		remotefile.setLocalFilepath(wpconf.getOutputfile());
		remotefile.execute();
		// delete temp local wp-config.php
		File file = new File(wpconf.getOutputfile()).getAbsoluteFile();
		file.delete();
	}

	public String getOutputfile() {
		return wpconf.getOutputfile();
	}

	public void setOutputfile(String outputfile) {
		wpconf.setOutputfile(outputfile);
	}

	public String getEnvironmentProperties() {
		return wpconf.getEnvironmentProperties();
	}

	public void setEnvironmentProperties(String environmentPropertiesfile) {
		wpconf.setEnvironmentProperties(environmentPropertiesfile);
	}

	public String getDatabaseProperties() {
		return wpconf.getDatabaseProperties();
	}

	public void setDatabaseProperties(String databasePropertiesfile) {
		wpconf.setDatabaseProperties(databasePropertiesfile);
	}

	public String getKeysProperties() {
		return wpconf.getKeysProperties();
	}

	public void setKeysProperties(String keysPropertiesfile) {
		wpconf.setKeysProperties(keysPropertiesfile);
	}

	public boolean isLoadNewKeys() {
		return wpconf.isLoadNewKeys();
	}

	public void setLoadNewKeys(boolean loadNewKeys) {
		wpconf.setLoadNewKeys(loadNewKeys);
	}
	
	public String getRemotePath() {
		return remotefile.getRemotePath();
	}

	public void setRemotePath(String remotePath) {
		remotefile.setRemotePath(remotePath);
	}
	
	public void setAlternateCron(boolean alternateCron) {
		wpconf.setAlternateCron(alternateCron);
	}

	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}
}
