package net.supervillainhq.php;

public class PhpIncludeFile {
	private String filepath;
	private String includeMethod;

	public String getFilepath() {
		return filepath;
	}

	public void setFilepath(String filepath) {
		this.filepath = filepath;
	}

	public String getIncludeMethod() {
		return includeMethod;
	}

	public void setIncludeMethod(String includeMethod) {
		this.includeMethod = includeMethod;
	}
}
