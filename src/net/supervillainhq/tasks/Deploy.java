package net.supervillainhq.tasks;

import java.util.ArrayList;
import java.util.Iterator;

import net.supervillainhq.SshConfig;
import net.supervillainhq.backup.UpdateBackupDatabaseScript;
import net.supervillainhq.backup.UpdateBackupUploadsScript;
import net.supervillainhq.deployment.RemoteSymlink;
import net.supervillainhq.deployment.SshCommand;
import net.supervillainhq.deployment.UpdateRemoteFile;
import net.supervillainhq.deployment.UpdateRemoteFiles;
import net.supervillainhq.deployment.UpdateRemoteFolder;
import net.supervillainhq.deployment.wordpress.UpdateRemoteConfig;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;

/**
 * Basic deploy-task customized for Zupa requirements
 * 
 * @author zr-ank
 *
 */
public class Deploy extends Task {
	private SshConfig sshconfig;
	private String sshconfigpath;
	private ArrayList<SshCommand> sshcommands;
	
	
	public Deploy(){
		sshcommands = new ArrayList<SshCommand>();
	}
	
	public void execute(){
		Project project = getProject();
		for (Iterator<SshCommand> iterator = sshcommands.iterator(); iterator.hasNext();) {
			SshCommand command = (SshCommand) iterator.next();
			command.setProject(project);
			command.setSshconfigpath(sshconfigpath);
			try {
				command.execute();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				break;
			}
		}
	}
	
	public void addConfiguredSshCommand(SshCommand command){
		sshcommands.add(command);
	}
	public void addConfiguredUpdateRemoteFolder(UpdateRemoteFolder command){
		sshcommands.add(command);
	}
	public void addConfiguredUpdateRemoteFile(UpdateRemoteFile command){
		sshcommands.add(command);
	}
	public void addConfiguredUpdateRemoteFiles(UpdateRemoteFiles command){
		sshcommands.add(command);
	}
	public void addConfiguredUpdateRemoteConfig(UpdateRemoteConfig command){
		sshcommands.add(command);
	}
	public void addConfiguredRemoteSymlink(RemoteSymlink command){
		sshcommands.add(command);
	}
	public void addConfiguredUpdateBackupDatabaseScript(UpdateBackupDatabaseScript command){
		sshcommands.add(command);
	}
	public void addConfiguredUpdateBackupUploadsScript(UpdateBackupUploadsScript command){
		sshcommands.add(command);
	}
	
	public SshConfig getSshconfig() {
		return sshconfig;
	}
	public void setSshconfig(SshConfig sshconfig) {
		this.sshconfig = sshconfig;
	}
	
	public String getSshconfigpath() {
		return sshconfigpath;
	}
	public void setSshconfigpath(String sshconfigpath) {
		this.sshconfigpath = sshconfigpath;
	}
}
