package net.supervillainhq.tasks.db;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline.Argument;

/**
 * Dump a table from 2 different mysql-servers and diff the contents of the
 * 2 output files.
 * 
 * @author ak
 *
 */
public class DiffTableContent extends Task {
	private String localDatabaseTable;
	private String remoteDatabaseTable;
	private String localDbConfig;
	private String remoteDbConfig;
	private String outputDirpath;

	public void execute(){
		Project project = getProject();
		if(null==outputDirpath){
			outputDirpath = project.getBaseDir() + "/data";
		}
		String localOutput = outputDirpath + "/local";
		String remoteOutput = outputDirpath + "/remote";
		String diffOutput = outputDirpath + "/diff.txt";
		DumpTable localTableDump = new DumpTable(getLocalDatabaseTable());
		localTableDump.setDatabaseConfig(localDbConfig);
		localTableDump.setOutputFilepath(localOutput);
		localTableDump.execute();
		DumpTable remoteTableDump = new DumpTable(getRemoteDatabaseTable());
		remoteTableDump.setDatabaseConfig(remoteDbConfig);
		remoteTableDump.setOutputFilepath(remoteOutput);
		remoteTableDump.execute();
		// create and execute command
		ExecTask exec = new ExecTask();
		exec.setProject(project);
		exec.setDir(project.getBaseDir());
		exec.setExecutable("diff");
		
		Argument arg;
		arg = exec.createArg();
		arg.setLine(localOutput);
		arg = exec.createArg();
		arg.setLine(remoteOutput);
		
		arg = exec.createArg();
		arg.setLine("--result-file=" + diffOutput);
		
		exec.execute();
	}

	public String getRemoteDatabaseTable() {
		return remoteDatabaseTable;
	}

	public void setRemoteDatabaseTable(String remoteDatabaseTable) {
		this.remoteDatabaseTable = remoteDatabaseTable;
	}

	public String getLocalDatabaseTable() {
		return localDatabaseTable;
	}

	public void setLocalDatabaseTable(String localDatabaseTable) {
		this.localDatabaseTable = localDatabaseTable;
	}

	public String getRemoteDbConfig() {
		return remoteDbConfig;
	}

	public void setRemoteDbConfig(String remoteDbConfig) {
		this.remoteDbConfig = remoteDbConfig;
	}

	public String getLocalDbConfig() {
		return localDbConfig;
	}

	public void setLocalDbConfig(String localDbConfig) {
		this.localDbConfig = localDbConfig;
	}

	public String getOutputDirpath() {
		return outputDirpath;
	}

	public void setOutputDirpath(String outputDirpath) {
		this.outputDirpath = outputDirpath;
	}
}
