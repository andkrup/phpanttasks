package net.supervillainhq.tasks.db;

import java.io.File;

import net.supervillainhq.db.DatabaseConfig;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.Task;
import org.apache.tools.ant.taskdefs.ExecTask;
import org.apache.tools.ant.types.Commandline.Argument;

public class DumpTable extends Task{
	protected Project project;
	protected String databaseConfigFilepath;
	private String tablename;
	private String outputFilepath;
	private DatabaseConfig dbConfig;

	public DumpTable(){
		this(null);
	}
	public DumpTable(String tablename){
		this.setTablename(tablename);
	}
	public void execute(){
		Project project = getProject();
		dbConfig = DatabaseConfig.fromConfigFile(project, new File(databaseConfigFilepath));
		// create and execute command
		ExecTask exec = new ExecTask();
		exec.setProject(project);
		exec.setDir(project.getBaseDir());
		exec.setExecutable("/opt/local/bin/mysqldump5");
		
		Argument arg;
		arg = exec.createArg();
		arg.setLine("-u" + dbConfig.username);
		arg = exec.createArg();
		arg.setLine("-p" + dbConfig.password);
		arg = exec.createArg();
		arg.setLine("--host=" + dbConfig.host);
		arg = exec.createArg();
		arg.setLine("--port=" + dbConfig.port);
		
		arg = exec.createArg();
		arg.setLine(dbConfig.database);
		
		arg = exec.createArg();
		arg.setLine(tablename);

		arg = exec.createArg();
		arg.setLine("--result-file=" + getOutputFilepath());
		
		exec.execute();

	}
	public String getDatabaseConfig() {
		return databaseConfigFilepath;
	}
	public void setDatabaseConfig(String path) {
		this.databaseConfigFilepath = path;
	}
	public String getOutputFilepath() {
		return outputFilepath;
	}
	public void setOutputFilepath(String outputFilepath) {
		this.outputFilepath = outputFilepath;
	}
	public String getTablename() {
		return tablename;
	}
	public void setTablename(String tablename) {
		this.tablename = tablename;
	}
}
