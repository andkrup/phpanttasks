package net.supervillainhq.tasks.wordpress;

import java.util.ArrayList;

import net.supervillainhq.deployment.wordpress.UpdateWordpressConfig;
import net.supervillainhq.php.PhpConst;
import net.supervillainhq.php.PhpIncludeFile;
import net.supervillainhq.wordpress.DatabaseCredentials;
import net.supervillainhq.wordpress.WordpressEnvironment;

import org.apache.tools.ant.Task;

/**
 * Updates or creates a wp-config.php file by parsing a properties file.
 * 
 * @author zr-ank
 *
 */
public class UpdateConfig extends Task {
	private UpdateWordpressConfig updatewpconf;
	private String lang;
	private String wpDebug;
	private ArrayList<PhpConst> phpConstants;
	private ArrayList<PhpIncludeFile> phpIncludefiles;
	private String inner;
	
	public UpdateConfig(){
		updatewpconf = new UpdateWordpressConfig(getProject());
		phpConstants = new ArrayList<PhpConst>();
		phpIncludefiles = new ArrayList<PhpIncludeFile>();
	}
	
	public void execute(){
		updatewpconf.setProject(getProject());
		updatewpconf.update(lang, wpDebug, inner, phpConstants, phpIncludefiles);
	}
	
	public WordpressEnvironment getWordpressEnvironment() {
		return updatewpconf.getWordpressEnvironment();
	}
	public void setWordpressEnvironment(WordpressEnvironment wpEnv) {
		updatewpconf.setWordpressEnvironment(wpEnv);
	}

	public String getOutputfile() {
		return updatewpconf.getOutputfile();
	}

	public void setOutputfile(String outputfile) {
		updatewpconf.setOutputfile(outputfile);
	}

	public DatabaseCredentials getDatabaseCredentials() {
		return updatewpconf.getDatabaseCredentials();
	}

	public void setDatabaseCredentials(DatabaseCredentials dbCreds) {
		updatewpconf.setDatabaseCredentials(dbCreds);
	}

	public String getEnvironmentProperties() {
		return updatewpconf.getEnvironmentProperties();
	}

	public void setEnvironmentProperties(String environmentPropertiesfile) {
		updatewpconf.setEnvironmentProperties(environmentPropertiesfile);
	}

	public String getDatabaseProperties() {
		return updatewpconf.getDatabaseProperties();
	}

	public void setDatabaseProperties(String databasePropertiesfile) {
		updatewpconf.setDatabaseProperties(databasePropertiesfile);
	}

	public String getKeysProperties() {
		return updatewpconf.getKeysProperties();
	}

	public void setKeysProperties(String keysPropertiesfile) {
		updatewpconf.setKeysProperties(keysPropertiesfile);
	}

	public WordpressKeys getWordpressKeys() {
		return updatewpconf.getWordpressKeys();
	}

	public void setWordpressKeys(WordpressKeys wpKeys) {
		updatewpconf.setWordpressKeys(wpKeys);
	}
	
	public void setUseAlternateCron(boolean useAlternateCron) {
		updatewpconf.setAlternateCron(useAlternateCron);
	}
	
	public void setUsePathConstants(boolean usePathConstants) {
		updatewpconf.setUsePathConstants(usePathConstants);
	}

	public boolean isLoadNewKeys() {
		return updatewpconf.isLoadNewKeys();
	}

	public void setLoadNewKeys(boolean loadNewKeys) {
		updatewpconf.setLoadNewKeys(loadNewKeys);
	}
	
	public String getLang() {
		return lang;
	}

	public void setLang(String lang) {
		this.lang = lang;
	}

	public String getWpDebug() {
		return wpDebug;
	}

	public void setWpDebug(String wpDebug) {
		this.wpDebug = wpDebug;
	}
	
	public void addConfiguredPhpConst(PhpConst phpConstant){
		phpConstants.add(phpConstant);
	}
	
	public void addConfiguredPhpIncludeFile(PhpIncludeFile phpIncludefile){
		phpIncludefiles.add(phpIncludefile);
	}
	
    public void addText(String text) {
        inner = text;
    }
   }
