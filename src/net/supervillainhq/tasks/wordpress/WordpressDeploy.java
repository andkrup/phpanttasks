package net.supervillainhq.tasks.wordpress;

import java.io.File;

import net.supervillainhq.tasks.Deploy;
import net.supervillainhq.wordpress.WordpressEnvironment;

import org.apache.tools.ant.types.FileSet;

public class WordpressDeploy extends Deploy {
	// path to properties file with ssh-credentials 
	private String configFile;
	// list of files to rsync
	private FileSet files;
	// rsync location
	private File rsyncPath;
	private WordpressEnvironment wpEnv;
	
	public WordpressDeploy(){
		wpEnv = new WordpressEnvironment();
	}
	
	public void execute(){}
}
