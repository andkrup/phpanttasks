package net.supervillainhq.tasks.wordpress;

import org.apache.tools.ant.Project;

public class WordpressKeys {
	private String authKey;
	private String secureAuthKey;
	private String loggedInKey;
	private String nonceKey;
	private String authSalt;
	private String secureAuthSalt;
	private String loggedInSalt;
	private String nonceSalt;

	public static WordpressKeys fromPropertyfile(Project project, String keysPropertiesfile) {
		return new WordpressKeysBuilder(project).createFromPropertyFile(keysPropertiesfile);
	}

	/**
	 * Uses the wordpress service at https://api.wordpress.org/secret-key/1.1/salt/ to generate new
	 * keys and salts
	 * @param project
	 * @return
	 */
	public static WordpressKeys loadNewKeys(Project project) {
		return new WordpressKeysBuilder(project).loadNewKeys();
	}
	public static WordpressKeys createNewKeysPropertyfile(Project project, String outputfile) {
		WordpressKeysBuilder builder = new WordpressKeysBuilder(project);
		WordpressKeys keys = builder.loadNewKeys();
		builder.saveKeys(outputfile);
		return keys;
	}

	public String getAuthKey() {
		return authKey;
	}

	public void setAuthKey(String authKey) {
		this.authKey = authKey;
	}

	public String getSecureAuthKey() {
		return secureAuthKey;
	}

	public void setSecureAuthKey(String secureAuthKey) {
		this.secureAuthKey = secureAuthKey;
	}

	public String getLoggedInKey() {
		return loggedInKey;
	}

	public void setLoggedInKey(String loggedInKey) {
		this.loggedInKey = loggedInKey;
	}

	public String getNonceKey() {
		return nonceKey;
	}

	public void setNonceKey(String nonceKey) {
		this.nonceKey = nonceKey;
	}

	public String getAuthSalt() {
		return authSalt;
	}

	public void setAuthSalt(String authSalt) {
		this.authSalt = authSalt;
	}

	public String getSecureAuthSalt() {
		return secureAuthSalt;
	}

	public void setSecureAuthSalt(String secureAuthSalt) {
		this.secureAuthSalt = secureAuthSalt;
	}

	public String getLoggedInSalt() {
		return loggedInSalt;
	}

	public void setLoggedInSalt(String loggedInSalt) {
		this.loggedInSalt = loggedInSalt;
	}

	public String getNonceSalt() {
		return nonceSalt;
	}

	public void setNonceSalt(String nonceSalt) {
		this.nonceSalt = nonceSalt;
	}
}
