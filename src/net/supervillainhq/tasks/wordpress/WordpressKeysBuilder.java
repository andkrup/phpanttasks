package net.supervillainhq.tasks.wordpress;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import net.supervillainhq.FileUtil;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Property;

public class WordpressKeysBuilder extends Property {

	public WordpressKeysBuilder(Project project) {
		setProject(project);
	}

	public WordpressKeys createFromPropertyFile(String keysPropertiesfile) {
		loadFile(new File(keysPropertiesfile));
		Project project = getProject();
		WordpressKeys wpKeys = new WordpressKeys();
		wpKeys.setAuthKey(project.getProperty("wp.auth_key"));
		wpKeys.setSecureAuthKey(project.getProperty("wp.secure_auth_key"));
		wpKeys.setLoggedInKey(project.getProperty("wp.logged_in_key"));
		wpKeys.setNonceKey(project.getProperty("wp.nonce_key"));
		wpKeys.setAuthSalt(project.getProperty("wp.auth_salt"));
		wpKeys.setSecureAuthSalt(project.getProperty("wp.secure_auth_salt"));
		wpKeys.setLoggedInSalt(project.getProperty("wp.logged_in_salt"));
		wpKeys.setNonceSalt(project.getProperty("wp.nonce_salt"));
		return wpKeys;
	}
	
	private String parseKey(String line){
		if(line.startsWith("define('AUTH_KEY'")){
			return "wp.auth_key";
		}
		else if(line.startsWith("define('SECURE_AUTH_KEY'")){
			return "wp.secure_auth_key";
		}
		else if(line.startsWith("define('LOGGED_IN_KEY'")){
			return "wp.logged_in_key";
		}
		else if(line.startsWith("define('NONCE_KEY'")){
			return "wp.nonce_key";
		}
		else if(line.startsWith("define('AUTH_SALT'")){
			return "wp.auth_salt";
		}
		else if(line.startsWith("define('SECURE_AUTH_SALT'")){
			return "wp.secure_auth_salt";
		}
		else if(line.startsWith("define('LOGGED_IN_SALT'")){
			return "wp.logged_in_salt";
		}
		else if(line.startsWith("define('NONCE_SALT'")){
			return "wp.nonce_salt";
		}
		return null;
	}
	
	public void saveKeys(String outputfile){
		Project project = getProject();
		String contents = "";
		contents += "wp.auth_key=" + project.getProperty("wp.auth_key") + "\n";
		contents += "wp.secure_auth_key=" + project.getProperty("wp.secure_auth_key") + "\n";
		contents += "wp.logged_in_key=" + project.getProperty("wp.logged_in_key") + "\n";
		contents += "wp.nonce_key=" + project.getProperty("wp.nonce_key") + "\n";
		contents += "wp.auth_salt=" + project.getProperty("wp.auth_salt") + "\n";
		contents += "wp.secure_auth_salt=" + project.getProperty("wp.secure_auth_salt") + "\n";
		contents += "wp.logged_in_salt=" + project.getProperty("wp.logged_in_salt") + "\n";
		contents += "wp.nonce_salt=" + project.getProperty("wp.nonce_salt") + "\n";
		FileUtil.writeToFile(contents, outputfile);
	}

	public WordpressKeys loadNewKeys(){
		WordpressKeys wpKeys = new WordpressKeys();
		Project project = getProject();
		URL wpKeyService = null;
		try {
			wpKeyService = new URL("https://api.wordpress.org/secret-key/1.1/salt/");
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if(null!=wpKeyService){
			BufferedReader reader = null;
			try {
				reader = new BufferedReader(new InputStreamReader(wpKeyService.openStream(), "UTF-8"));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(null!=reader){
				try {
					String line;
					String key;
					String value;
					while ((line = reader.readLine()) != null){
						key = parseKey(line);
						if(null!=key){
							value = line.substring(28, 64+28);
							project.setProperty(key, value);
						}
					}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		wpKeys.setAuthKey(project.getProperty("wp.auth_key"));
		wpKeys.setSecureAuthKey(project.getProperty("wp.secure_auth_key"));
		wpKeys.setLoggedInKey(project.getProperty("wp.logged_in_key"));
		wpKeys.setNonceKey(project.getProperty("wp.nonce_key"));
		wpKeys.setAuthSalt(project.getProperty("wp.auth_salt"));
		wpKeys.setSecureAuthSalt(project.getProperty("wp.secure_auth_salt"));
		wpKeys.setLoggedInSalt(project.getProperty("wp.logged_in_salt"));
		wpKeys.setNonceSalt(project.getProperty("wp.nonce_salt"));
		return wpKeys;
	}
}
