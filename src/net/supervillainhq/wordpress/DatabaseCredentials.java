package net.supervillainhq.wordpress;

import org.apache.tools.ant.Project;

public class DatabaseCredentials {

	private String user;
	private String pass;
	private String database;
	private String host;
	private String charset;
	private String collate;
	private String prefix;
	
	public DatabaseCredentials(){
		setCharset("utf8");
		setCollate("");
		setUser("wordpress");
		setPass("wordpress");
		setHost("localhost");
		setDatabase("Wordpress");
		setPrefix("wp_");
	}
	
	
	public static DatabaseCredentials fromPropertyfile(Project project, String path){
		return new DatabaseCredentialsBuilder(project).createFromPropertyFile(path);
	}
	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getDatabase() {
		return database;
	}
	public void setDatabase(String database) {
		this.database = database;
	}
	public String getHost() {
		return host;
	}
	public void setHost(String host) {
		this.host = host;
	}
	public String getCharset() {
		return charset;
	}
	public void setCharset(String charset) {
		this.charset = charset;
	}
	public String getCollate() {
		return collate;
	}
	public void setCollate(String collate) {
		this.collate = collate;
	}

	public String getPrefix() {
		return prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}
}
