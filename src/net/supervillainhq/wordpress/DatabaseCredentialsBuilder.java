package net.supervillainhq.wordpress;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Property;

public class DatabaseCredentialsBuilder extends Property {
	
	public DatabaseCredentialsBuilder(Project project) {
		setProject(project);
	}
	
	public DatabaseCredentials createFromPropertyFile(String path) {
		loadFile(new File(path));
		Project project = getProject();
		DatabaseCredentials creds = new DatabaseCredentials();
		creds.setCharset(project.getProperty("db.charset"));
		creds.setCollate(project.getProperty("db.collate"));
		creds.setDatabase(project.getProperty("db.database"));
		creds.setHost(project.getProperty("db.host"));
		creds.setPass(project.getProperty("db.pass"));
		creds.setPrefix(project.getProperty("db.prefix"));
		creds.setUser(project.getProperty("db.user"));
		
		return creds;
	}
}
