package net.supervillainhq.wordpress;

import org.apache.tools.ant.Project;


public class WordpressEnvironment {
	private String pluginsPath;
	private String pluginsUrl;
	private String themesPath;
	private String themesUrl;
	private String uploadsPath;
	private String uploadsUrl;
	
	private String language;
	private boolean alternateCron;
	private boolean debug;
	private boolean fileEditDisabled;
	private boolean fileModDisabled;
	
	
	public WordpressEnvironment() {
		this("/var/www");
	}
	public WordpressEnvironment(String pluginsPath) {
		this(pluginsPath, "//wp-content/plugins");
	}
	public WordpressEnvironment(String pluginsPath, String pluginsUrl) {
		this(pluginsPath, pluginsUrl, "wp-content/themes");
	}
	public WordpressEnvironment(String pluginsPath, String pluginsUrl, String themesPath) {
		this(pluginsPath, pluginsUrl, themesPath, "//wp-content/themes");
	}
	public WordpressEnvironment(String pluginsPath, String pluginsUrl, String themesPath, String themesUrl) {
		this(pluginsPath, pluginsUrl, themesPath, themesUrl, "wp-content/uploads");
	}
	public WordpressEnvironment(String pluginsPath, String pluginsUrl, String themesPath, String themesUrl, String uploadsPath) {
		this(pluginsPath, pluginsUrl, themesPath, themesUrl, uploadsPath, "/wp-content/uploads");
	}
	public WordpressEnvironment(String pluginsPath, String pluginsUrl, String themesPath, String themesUrl, String uploadsPath, String uploadsUrl) {
		setPluginsPath(pluginsPath);
		setPluginsUrl(pluginsUrl);
		setThemesPath(themesPath);
		setThemesUrl(themesUrl);
		setUploadsPath(uploadsPath);
		setUploadsUrl(uploadsUrl);
	}
	
	public static WordpressEnvironment fromPropertyfile(Project project, String path){
		return new WordpressEnvironmentBuilder(project).createFromPropertyFile(path);
	}

	
	public String getPluginsPath() {
		return pluginsPath;
	}
	public void setPluginsPath(String pluginsPath) {
		this.pluginsPath = pluginsPath;
	}
	public String getPluginsUrl() {
		return pluginsUrl;
	}
	public void setPluginsUrl(String pluginsUrl) {
		this.pluginsUrl = pluginsUrl;
	}
	public String getThemesPath() {
		return themesPath;
	}
	public void setThemesPath(String themesPath) {
		this.themesPath = themesPath;
	}
	public String getThemesUrl() {
		return themesUrl;
	}
	public void setThemesUrl(String themesUrl) {
		this.themesUrl = themesUrl;
	}
	public String getUploadsPath() {
		return uploadsPath;
	}
	public void setUploadsPath(String uploadsPath) {
		this.uploadsPath = uploadsPath;
	}
	public String getUploadsUrl() {
		return uploadsUrl;
	}
	public void setUploadsUrl(String uploadsUrl) {
		this.uploadsUrl = uploadsUrl;
	}
	public String getLanguage() {
		return language;
	}
	public void setLanguage(String language) {
		this.language = language;
	}
	public boolean isDebug() {
		return debug;
	}
	public void setDebug(boolean debug) {
		this.debug = debug;
	}
	public boolean isAlternateCron() {
		return alternateCron;
	}
	public void setAlternateCron(boolean alternateCron) {
		this.alternateCron = alternateCron;
	}
	public boolean getFileEditDisabled() {
		return this.fileEditDisabled;
	}
	public void setFileEditDisabled(boolean value) {
		this.fileEditDisabled = value;
	}
	public boolean getFileModsDisabled() {
		return this.fileModDisabled;
	}
	public void getFileModsDisabled(boolean value) {
		this.fileModDisabled = value;
	}
}
