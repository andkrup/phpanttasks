package net.supervillainhq.wordpress;

import java.io.File;

import org.apache.tools.ant.Project;
import org.apache.tools.ant.taskdefs.Property;

public class WordpressEnvironmentBuilder extends Property{

	public WordpressEnvironmentBuilder(Project project) {
		setProject(project);
	}

	public WordpressEnvironment createFromPropertyFile(String path) {
		loadFile(new File(path));
		Project project = getProject();
		WordpressEnvironment wpEnv = new WordpressEnvironment();
		String wpDebug = project.getProperty("wp.debug");
		wpEnv.setDebug(null!=wpDebug ? wpDebug.equals("true") : false);
		String wpAlternateCron = project.getProperty("wp.alternatecron");
		wpEnv.setAlternateCron(null!=wpAlternateCron ? wpAlternateCron.equals("true") : false);
		wpEnv.setLanguage(project.getProperty("wp.language"));
		wpEnv.setPluginsPath(project.getProperty("wp.pluginspath"));
		wpEnv.setPluginsUrl(project.getProperty("wp.pluginsurl"));
		wpEnv.setThemesPath(project.getProperty("wp.themespath"));
		wpEnv.setThemesUrl(project.getProperty("wp.themesurl"));
		wpEnv.setUploadsPath(project.getProperty("wp.uploadspath"));
		wpEnv.setUploadsUrl(project.getProperty("wp.uploadsurl"));
		return wpEnv;
	}

}
